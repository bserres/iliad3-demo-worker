from logging import Logger, getLogger

from arkindex_worker.models import Element
from arkindex_worker.worker import ElementsWorker

logger: Logger = getLogger(__name__)


class MyWoker(ElementsWorker):
    def process_element(self, element: Element) -> None:
        logger.info(f"TEST! Currently processing element ({element.id})")


def main() -> None:
    MyWorker(description="My Demo ML worker, by ILIAD3").run()


if __name__ == "__main__":
    main()
